package com.bcexpress.model;

/**
 * <p>
 * A class that be will the basis what data must be in inputs and outputs for validations.
 * </p>
 * 
 * <br/>
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class CustomerVO {
	private int id;
	
    @NotNull(message = "First name must not be empty.")
    @Size(max=20, message = "First name is too long.")
    @Pattern(regexp="[^0-9]*" , message = "First name must contain no numeric characters.")
    @Pattern(regexp="[^!@#$%^&*()_+{}?,';:-=.<+>/+|]*", message = "First name must contain no special characters.")
    @NotBlank(message = "First name must not be empty.")
	private String firstName;
    
    @NotNull(message = "Last name must not be empty.")
    @Size(max=20, message = "Last name is too long.")
    @Pattern(regexp="[^0-9]*" , message = "Last name must contain no numeric characters.")
    @Pattern(regexp="[^!@#$%^&*()_+{}?,';:-=.<+>/+|]*", message = "Last name must contain no special characters.")
    @NotBlank(message = "Last name must not be empty.")
	private String lastName;
    
    @NotNull(message = "Address must not be empty.")
    @Size(max=40, message = "Address is too long.")
    @NotBlank(message = "Address must not be empty.")
	private String address;
    
    @NotNull(message = "Gender must not be empty.")
    @Size(max=1, message = "Gender must be M or F.")
    @Pattern(regexp="[^0-9]*" , message = "Gender must be M or F.")
    @Pattern(regexp="[^!@#$%^&*()_+{}?,';:-=.<+>/+|]*", message = "Gender must be M or F.")
    @NotBlank(message = "Gender must not be empty.")
	private String gender;
    
    @NotNull(message = "age must not be empty.")
    @Pattern(regexp="[^!@#$%^&*()_+{}?,';:-=.<+>/+|]*", message = "age must be numeric.")
    @NotBlank(message = "age must not be empty.")
    @Pattern(regexp="[^A-Za-z]*", message = "age must be numeric.")//alphabetic
	private String age;
	
	/**
	 * Gets id of the customer
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Sets id of the customer
	 * @param id int variable 1
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Returns the first name of the customer
	 * @return firstName String
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * Sets the first name of the customer
	 * @param firstName String variable 1
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * Returns the last name of the customer
	 * @return lastName String
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * Sets the last name of the customer
	 * @param lastName String variable 1
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * Returns the address of the customer
	 * @return address String 
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * Sets the address of the customer
	 * @param address String variable 1
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * Returns the gender of the customer
	 * @return gender String 
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * Sets the gender of the customer
	 * @param gender String variable 1
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * Gets the age of the customer
	 * @return age String 
	 */
	public String getAge() {
		return age;
	}
	/**
	 * Sets the age of the customer
	 * @param age String variable 1
	 */
	public void setAge(String age) {
		this.age = age;
	}

	
	
}
