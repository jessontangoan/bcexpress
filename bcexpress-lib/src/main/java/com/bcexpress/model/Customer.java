package com.bcexpress.model;

/**
 * <p>
 * A class that will the bases what data must be in inputs and outputs.
 * </p>
 * 
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Component;
@Component
@Entity
@Table(name="customer")
public class Customer {

	@Id
	@Column(name="id")
	@GeneratedValue
	private int id;
	
	@Column(name="firstName")
//	@NotEmpty(message="Test")
//	@Min(value=1, message="Age can't be less than 120")
//	@Max(value=120, message="Age can't be more than 120")
	private String firstName;
	
//	@Min(value=1, message="Age can't be less than 120")
//	@Max(value=120, message="Age can't be more than 120")
//	@NotEmpty(message="Test")
	@Column(name="lastName")
	private String lastName;
	
//	@Min(value=1, message="Age can't be less than 120")
//	@Max(value=120, message="Age can't be more than 120")
//	@NotEmpty(message="Test")
//	@Column(name="address")
	private String address;
	
//	@Max(value=1, message="Age can't be less than 120")
//	@Column(name="gender")
//	@NotEmpty(message="Test")
	private String gender;
	
//	@Min(value=1, message="Age can't be less than 120")
//	@Max(value=120, message="Age can't be more than 120")
//	@NotEmpty(message="Test")
//	@Column(name="age")
	private int age;
	
	
	/**
     * Invoked during instantiation of Customer.
     */
	public Customer() {
		super();
	}

	
	/**
	 * Gets id of the customer
	 * @return id
	 */
	public int getId() {
		return id;
	}
	/**
	 * Sets id of the customer
	 * @param id int variable 1
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * Returns the first name of the customer
	 * @return firstName String
	 */
	public String getFirstName() {
		return firstName;
	}
	/**
	 * Sets the first name of the customer
	 * @param firstName String variable 1
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * Returns the last name of the customer
	 * @return lastName String
	 */
	public String getLastName() {
		return lastName;
	}
	/**
	 * Sets the last name of the customer
	 * @param lastName String variable 1
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	/**
	 * Returns the address of the customer
	 * @return address String 
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * Sets the address of the customer
	 * @param address String variable 1
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * Returns the gender of the customer
	 * @return gender String 
	 */
	public String getGender() {
		return gender;
	}
	/**
	 * Sets the gender of the customer
	 * @param gender String variable 1
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}
	/**
	 * Gets the age of the customer
	 * @return age String 
	 */
	public int getAge() {
		return age;
	}
	/**
	 * Sets the age of the customer
	 * @param age String variable 1
	 */
	public void setAge(int age) {
		this.age = age;
	}
	

	
}
