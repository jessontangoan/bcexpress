package com.bcexpress.facade;

/**
 * <p> 
 * A class that manages what services will be used
 * </p>
 * 
 * Created on January 29, 2019
 * 
 * @author TeamJ31 
 */
import com.bcexpress.model.Customer;

public interface CustomerFacade {

	/**
     * For saving data to database
     * @param customer Customer variable 1
     */
	public void save(Customer customer);
	
	/**
     * For Retrieving data from database
     * @param id int variable 1
     * @return Customer object
     */
	public Customer get(int id);

}
