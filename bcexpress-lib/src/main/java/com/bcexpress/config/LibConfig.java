package com.bcexpress.config;
/**
 * <p> 
 * A class that consists of Configurations for SQL/Database.
 * </p>
 * 
 * <br/>
 * 
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.bcexpress.repository")
@ComponentScan({"com.bcexpress.model","com.bcexpress.service"})
public class LibConfig {
	
	private static final String PROPERTY_NAME_DATABASE_DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String PROPERTY_NAME_DATABASE_PASSWORD = "abcd123";
	private static final String PROPERTY_NAME_DATABASE_URL = "jdbc:mysql://localhost:3306/customer_db";
	private static final String PROPERTY_NAME_DATABASE_USERNAME = "root";

	
	/**
	 * Description: Returns Local Container Entity Manager Factory Bean message
	 * @return message LocalContainerEntityManagerFactoryBean
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(mySQLDataSource());
		em.setPackagesToScan("com.bcexpress.model");
		
		JpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		em.setJpaProperties(additionalProperties());

		return em;
	}
	/**
	 * Description: Returns data source information
	 * @return mySQLDataSource DataSource
	 */
	@Bean
	public DataSource mySQLDataSource() {
		DriverManagerDataSource mySQLDataSource = new DriverManagerDataSource();
		mySQLDataSource.setDriverClassName(PROPERTY_NAME_DATABASE_DRIVER);
		mySQLDataSource.setUsername(PROPERTY_NAME_DATABASE_USERNAME);
		mySQLDataSource.setPassword(PROPERTY_NAME_DATABASE_PASSWORD);
		mySQLDataSource.setUrl(PROPERTY_NAME_DATABASE_URL);
		return mySQLDataSource;
	}
	/**
	 * Description: Returns Platform Transaction Manager information
	 * @return transactionManager PlatformTransactionManager
	 */
	@Bean
	public PlatformTransactionManager transactionManager(
			EntityManagerFactory emf) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(emf);

		return transactionManager;
	}
	/**
	 * Description: Returns Persistence ExceptionTranslation Post Processor information
	 * @return PersistenceExceptionTranslationPostProcessor
	 */
	@Bean
	public PersistenceExceptionTranslationPostProcessor exceptionTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	/**
	 * Description: Returns properties information
	 * @return properties object
	 */
	Properties additionalProperties() {
		Properties properties = new Properties();
		properties.setProperty("hibernate.hbm2ddl.auto", "validate"); //(create/validate)depending on the property set here; it will create or drop a table
		properties.setProperty("hibernate.dialect",
				"org.hibernate.dialect.MySQL5Dialect");

		return properties;
	}
}
