package com.bcexpress.repository;

/**
 * <p>
 * Interface class to use the JPA hibernate actions.
 * </p>
 * <br/>
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bcexpress.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long>{
	/**
	 * For Retrieving data from database
	 * @param id int - Customer ID
	 * @return Customer object
	 * */
	public Customer findById(int id);
}
