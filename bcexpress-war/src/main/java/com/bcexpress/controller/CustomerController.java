package com.bcexpress.controller;

/**
 * <p>
 * Accepts parameters for validations of data to be save in database and retrieve from database.
 * </p>
 *</br>
 * Created on January 29, 2019
 *
 * @author TeamJ3
 */
import javax.validation.Valid;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bcexpress.exceptions.CustomExceptions;
import com.bcexpress.exceptions.ErrorFormat;
import com.bcexpress.exceptions.ErrorHandlingGetCustomer;
import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.model.Customer;
import com.bcexpress.model.CustomerVO;

@Controller
@RequestMapping("/service")
public class CustomerController {

	@Autowired
	private CustomerFacade customerFacade;//To call services from bcexpress-lib
	
	static final String MESSAGE = "message";
	static final String DESCRIPTION = "description";
	static final String ID = "Invalid ID";
	
	/**
	 * Description: Catch errors and validate each variables that being pass that will be used for retrieving datas.
	 * @param id int - for customer id that will be validated. 
	 * @return message ResponseEntity - indicate which variable has errors or if the id is valid and the transaction if successful.
	 * @return description ResponseEntity - indicate the specific problem of the message.
	 * @throws e NumberFormatException
	 * @throws e NullPointerException
	 */
	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity getCustomer(@PathVariable String id){
		
		
		ErrorHandlingGetCustomer errorHandlingGetCustomer =new ErrorHandlingGetCustomer();
		ErrorFormat errorMessage;
		try{	
			
			if(id.contains(".")){
				
				errorMessage = errorHandlingGetCustomer.errorMessageFormat();
				errorMessage.setMessage(ID);
				errorMessage.setDescription(CustomExceptions.ID_NON_NUMERIC_DECIMAL_EXCEPTION);
				return  new ResponseEntity<ErrorFormat>(errorMessage,HttpStatus.BAD_REQUEST);
			}
			else if( (!id.matches("[0-9]+")) ) {
				
				errorMessage = errorHandlingGetCustomer.errorMessageFormat();
				errorMessage.setMessage(ID);
				errorMessage.setDescription(CustomExceptions.ID_NON_NUMERIC_ALPHA_EXCEPTION);
				return  new ResponseEntity<ErrorFormat>(errorMessage,HttpStatus.BAD_REQUEST);
			}
			int idInt = Integer.parseInt(id);
			Customer customer;
			customer=customerFacade.get(idInt);
			customer.getFirstName();
			return new ResponseEntity<Customer>(customer,HttpStatus.OK);
		}catch(NullPointerException e){
			JSONObject messageSuccess = new JSONObject();
			messageSuccess.put(MESSAGE,"No Record Found");
			return new ResponseEntity<JSONObject>(messageSuccess,HttpStatus.NOT_FOUND);
		}
	}

	/**
	 * Description: Catch errors and validate each variables that being pass that will be used for retrieving datas.
	 * @param id int variable 1 message ResponseEntity
	 * @return ResponseEntity object
	 */	
	@RequestMapping(value = "/get/", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public ResponseEntity getNullCustomer(){
		ErrorFormat errorMessage = new ErrorFormat();//Instantiate a class to consumes the input for validation.
		errorMessage.setMessage(ID);
		
		errorMessage.setDescription(CustomExceptions.ID_NULL_EMPTY_EXCEPTION);
	
		return  new ResponseEntity<ErrorFormat>(errorMessage,HttpStatus.BAD_REQUEST);
	}


	/**
	 * Description: Catch errors and validate each variables that being pass
	 * @param customerVO CustomerVO variable 1 - Object which contains the variables and data to be validated 
	 * @param validationError BindingResult variable 1 - An error message to be process its content to identify what are the errors.
	 * @return message ResponseEntity - indicate which variable has errors or if the variables are valid and indicate if transaction is successful.
	 * @return description ResponseEntity - indicate the specific problem of the message.
	 */	
	@RequestMapping(value = "/save", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
	@ResponseBody
	public ResponseEntity save(@Valid @RequestBody CustomerVO customerVO,BindingResult errorResult)  {
		JSONObject jsonResponse = new JSONObject();
		if(errorResult.hasErrors()){
			jsonResponse.put(MESSAGE, "Invalid "+errorResult.getFieldError().getField());
			jsonResponse.put(DESCRIPTION, errorResult.getFieldError().getDefaultMessage());
			return new ResponseEntity<JSONObject>(jsonResponse,HttpStatus.BAD_REQUEST);
		}
		
		if(Integer.parseInt(customerVO.getAge()) < 18 || Integer.parseInt(customerVO.getAge()) > 120){
			jsonResponse.put(MESSAGE, "Invalid age");
			jsonResponse.put(DESCRIPTION, CustomExceptions.AGE_NOT_IN_RANGE_EXCEPTION);
			return new ResponseEntity<JSONObject>(jsonResponse,HttpStatus.BAD_REQUEST);
		}
		if(!customerVO.getGender().equalsIgnoreCase("M")&& !customerVO.getGender().equalsIgnoreCase("F")){
			jsonResponse.put(MESSAGE, "Invalid Gender");
			jsonResponse.put(DESCRIPTION, CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION);
			return new ResponseEntity<JSONObject>(jsonResponse,HttpStatus.BAD_REQUEST);
		}
		Customer customerObj = new Customer();
		customerObj.setFirstName(customerVO.getFirstName());
		customerObj.setLastName(customerVO.getLastName());
		customerObj.setAddress(customerVO.getAddress());
		customerObj.setAge(Integer.parseInt(customerVO.getAge()));
		customerObj.setGender(customerVO.getGender());
		customerFacade.save(customerObj);
		jsonResponse.put(MESSAGE, "Customer Successfully Created");
		return new ResponseEntity<JSONObject>(jsonResponse,HttpStatus.CREATED);
	}
	
	
}
