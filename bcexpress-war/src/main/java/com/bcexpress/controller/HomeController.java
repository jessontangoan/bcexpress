package com.bcexpress.controller;

/**
 * <p>
 * To direct in home or starting point.
 * </p>
 * 
 * <br/>
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

	/**
	 * Description:For viewing the home page
	 * @param value param variable 1
	 */
	@RequestMapping(value="/")
	public ModelAndView test(HttpServletResponse response) {
		return new ModelAndView("home");
	}
	
}
