package com.bcexpress.exceptions;

/**
 * <p>
 * Customized error messages and exception to cater our assumptions .
 * </p>
 * 
 * <br/>
 * Created on January 29, 2019
 * 
 * @author TeamJ3
 */
public class CustomExceptions {

	/**
	 * Description: To test/send if the error message being customized returns the exact statement.
	 * @return FIRST_NAME_NULL_EMPTY_EXCEPTION Exception
	 */	
	public String sendException(){
		
		return  FIRST_NAME_NULL_EMPTY_EXCEPTION;
		}
			public static final String FIRST_NAME_NULL_EMPTY_EXCEPTION = "First name must not be empty.";
			public static final String FIRST_NAME_EMPTY_STRING_EXCEPTION = "First name must not be empty.";
			public static final String FIRST_NAME_EMPTY_EMPTY_EXCEPTION = "First name must not be empty.";
			public static final String FIRST_NAME_NUMERIC_EXCEPTION = "First name must contain no numeric characters.";
			public static final String FIRST_NAME_SPECIAL_EXCEPTION = "First name must contain no special characters.";
			public static final String FIRST_NAME_TOO_LONG_EXCEPTION = "First name is too long.";
			
			public static final String LAST_NAME_NULL_EMPTY_EXCEPTION = "Last name must not be empty.";
			public static final String LAST_NAME_EMPTY_STRING_EXCEPTION = "Last name must not be empty.";
			public static final String LAST_NAME_EMPTY_EMPTY_EXCEPTION = "Last name must not be empty.";
			public static final String LAST_NAME_NUMERIC_EXCEPTION = "Last name must contain no numeric characters.";
			public static final String LAST_NAME_SPECIAL_EXCEPTION = "Last name must contain no special characters.";
			public static final String LAST_NAME_TOO_LONG_EXCEPTION = "Last name is too long.";
			
			public static final String ADDRESS_NULL_EMPTY_EXCEPTION = "Address must not be empty.";
			public static final String ADDRESS_TOO_LONG_EXCEPTION = "Address is too long.";
			public static final String ADDRESS_EMPTY_EMPTY_EXCEPTION = "Address must not be empty.";
			public static final String ADDRESS_EMPTY_STRING_EXCEPTION = "Address must not be empty.";
			
			public static final String GENDER_NULL_EMPTY_EXCEPTION = "Gender must not be empty.";
			public static final String GENDER_EMPTY_EMPTY_EXCEPTION = "Gender must not be empty.";
			public static final String GENDER_INVALID_EMPTY_EXCEPTION = "Gender must be M or F.";
			
			public static final String AGE_NULL_EMPTY_EXCEPTION = "age must not be empty.";
			public static final String AGE_NON_NUMERIC_EXCEPTION = "age must be numeric.";
			public static final String AGE_NOT_IN_RANGE_EXCEPTION = "age not in allowable range.";
			public static final String AGE_EMPTY_EMPTY_EXCEPTION  ="age must not be empty.";
			
			public static final String ID_NULL_EMPTY_EXCEPTION = "ID must not be empty.";
			public static final String ID_NOT_FOUND_EXCEPTION = "No Record Found.";
			public static final String ID_NON_NUMERIC_ALPHA_EXCEPTION = "ID must not contain non-numeric character(s).";
			public static final String ID_NON_NUMERIC_SPECIAL_EXCEPTION = "ID must not contain non-numeric character(s).";
			public static final String ID_NON_NUMERIC_DECIMAL_EXCEPTION ="ID must not be decimal.";
	

}
