package com.bcexpress.junitTest;

/**
 * A test class to test the classes based from our Unit Test Plan.
 * Created on January 29, 2019
 * @author TeamJ3
 */
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bcexpress.controller.CustomerController;
import com.bcexpress.exceptions.CustomExceptions;
import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.model.Customer;
import com.bcexpress.service.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GetCustomerTest {
	
	private MockMvc mockMvc;
	private Customer customerObj = new Customer();
	private CustomerService customerService = new CustomerService();
	private ObjectMapper objectMapper;
	
	@Mock
	private CustomerFacade customerFacade;
	
	@InjectMocks
	private CustomerController customerController;
	
	/**
	 * Description: Method for Mockito initialization
	 */
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
		objectMapper = new ObjectMapper();
		
		
	}

	/**
	 * Description: Method to validate get customer service.
	 */
	@Test
	public void getCustomerValidTest() throws Exception{
		//mock the Facade
		//CustomerFacade customerFacade = Mockito.mock(CustomerFacade.class);
		
		customerObj.setAddress("Pagsabungan");
		customerObj.setAge(23);
		customerObj.setFirstName("Jesson");
		customerObj.setLastName("Tangoan");
		customerObj.setGender("M");
		
		//use the facade
		Mockito.when(customerFacade.get(5)).thenReturn(customerObj);
		
		
			mockMvc.perform(get("/service/get/5").contentType(MediaType.APPLICATION_JSON))//this will call customerFacade.get(5)
	    	.andExpect(status().isOk())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
	        .andExpect(jsonPath("$.firstName", is("Jesson")))
	        .andExpect(jsonPath("$.lastName", is("Tangoan")))
	        .andExpect(jsonPath("$.gender", is("M")))
	        .andExpect(jsonPath("$.age", is(23)))
	        .andExpect(jsonPath("$.address",is("Pagsabungan")));
		
	}
	
	/**
	 * Description: Method to validate get customer ID with special character
	 * @throws exception Exception
	 */
	@Test
	public void specialCharacterID() throws Exception{
		//mock the Facade
		//CustomerFacade customerFacade = Mockito.mock(CustomerFacade.class);
		
		customerObj.setAddress("Pagsabungan");
		customerObj.setAge(23);
		customerObj.setFirstName("Jesson");
		customerObj.setLastName("Tangoan");
		customerObj.setGender("M");
		
		//use the facade
		Mockito.when(customerFacade.get(5)).thenReturn(customerObj);
		
		
			mockMvc.perform(get("/service/get/1&%").contentType(MediaType.APPLICATION_JSON))//this will call customerFacade.get(5)
	    	.andExpect(status().is(400))
	    	.andExpect(jsonPath("$.message", is("Invalid ID")))
			.andExpect(jsonPath("$.description", is(CustomExceptions.ID_NON_NUMERIC_SPECIAL_EXCEPTION)));
		
	}
	
	/**
	 * Description: Method to validate get customer ID with non-numeric ID
	 * throws exception Exception
	 */
	@Test
	public void nonNumericID() throws Exception{
		//mock the Facade
		//CustomerFacade customerFacade = Mockito.mock(CustomerFacade.class);
		
		customerObj.setAddress("Pagsabungan");
		customerObj.setAge(23);
		customerObj.setFirstName("Jesson");
		customerObj.setLastName("Tangoan");
		customerObj.setGender("M");
		
		//use the facade
		Mockito.when(customerFacade.get(5)).thenReturn(customerObj);
		
		
			mockMvc.perform(get("/service/get/1234bz").contentType(MediaType.APPLICATION_JSON))//this will call customerFacade.get(5)
	    	.andExpect(status().is(400))
	    	.andExpect(jsonPath("$.message", is("Invalid ID")))
			.andExpect(jsonPath("$.description", is(CustomExceptions.ID_NON_NUMERIC_SPECIAL_EXCEPTION)));
		
	}
	
	
	/**
	 * Description: Method to validate get customer ID as null
	 * throws exception Exception
	 */
	@Test
	public void NullIDTest() throws Exception{
		//mock the Facade
		//CustomerFacade customerFacade = Mockito.mock(CustomerFacade.class);
		
			mockMvc.perform(get("/service/get/").contentType(MediaType.APPLICATION_JSON))//this will call customerFacade.get(5)
	    	.andExpect(status().isBadRequest())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
	        .andExpect(jsonPath("$.message", is("Invalid ID")))
			.andExpect(jsonPath("$.description", is(CustomExceptions.ID_NULL_EMPTY_EXCEPTION)));
		
	}
	
	/**
	 * Description: Method to validate if no record found for null customer
	 * throws exception Exception
	 */
	@Test
	public void getCustomerTestNoRecord() throws Exception{
		//mock the Facade
		//CustomerFacade customerFacade = Mockito.mock(CustomerFacade.class);
		
		customerObj = null;
		
		//use the facade
		Mockito.when(customerFacade.get(1111111)).thenReturn(customerObj);
		
		
			mockMvc.perform(get("/service/get/1111111").contentType(MediaType.APPLICATION_JSON))//this will call customerFacade.get(5)
	    	.andExpect(status().is(404))
	    	.andExpect(jsonPath("$.message", is("No Record Found")));
		
	}
	
	/**
	 * Description: Method to validate get customer ID with decimal characters
	 * throws exception Exception
	 */
	@Test
	public void decimalIDTest() throws Exception{
		//mock the Facade
		//CustomerFacade customerFacade = Mockito.mock(CustomerFacade.class);
		
			mockMvc.perform(get("/service/get/12.7.").contentType(MediaType.APPLICATION_JSON))//this will call customerFacade.get(5)
	    	.andExpect(status().isBadRequest())
	        .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
	        .andExpect(jsonPath("$.message", is("Invalid ID")))
			.andExpect(jsonPath("$.description", is(CustomExceptions.ID_NON_NUMERIC_DECIMAL_EXCEPTION)));
		
	}
	
	
	
	
	
}
