package com.bcexpress.junitTest;

/**
 * A class to test saving of customer object
 * Created on January 29, 2019
 * @author TeamJ3
 */
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bcexpress.controller.CustomerController;
import com.bcexpress.exceptions.CustomExceptions;
import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.model.CustomerVO;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SaveCustomerTest {
	
	private MockMvc mockMvc;
	private ObjectMapper objectMapper;
	
	@Mock
	private CustomerFacade customerFacade;
	
	@InjectMocks
	private CustomerController customerController;
	
	/**
	 * Description: Method invoked to initialize mock values
	 */
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
		objectMapper = new ObjectMapper();
		
		
	}
	
	
	/**
	 * Description: Test Method for successful first name validation
	 * @throws Exception object
	 */
	@Test
	public void validFirstNameTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setFirstName("Bilbo Abelik Guulime");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	/**
	 * Description: Test Method for successful address validation
	 * @throws Exception object
	 */
	@Test
	public void validAddressTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("M");
		cust.setAge("23");
		cust.setFirstName("Bilbo Abelik Guulime");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	/**
	 * Description: Test Method for successful address validation (boundary)
	 * @throws Exception object
	 */
	@Test
	public void validAddressTest2() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("8979 Wilson Circle Homestead");
		cust.setAge("23");
		cust.setFirstName("Bilbo Abelik Guulime");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	
	/**
	 * Description: Test Method for successful gender validation
	 * @throws Exception object
	 */
	@Test
	public void validGenderTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("8979 Wilson Circle Homestead");
		cust.setAge("23");
		cust.setFirstName("cebu");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	/**
	 * Description: Test Method for successful first name minimum validation
	 * @throws Exception object
	 */
	@Test
	public void minimumLengthFirstNameTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setFirstName("B");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	
	/**
	 * Description: Test Method for invalid first name - containing numeric
	 * @throws Exception object
	 */
	@Test
	public void numericFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setFirstName("bilbo23");
		cust.setGender("M");
		cust.setLastName("Baggins");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_NUMERIC_EXCEPTION)));
	
	}
	
	
	/**
	 * Description: Test Method for invalid first name - containing numeric
	 * @throws Exception object
	 */
	@Test
	public void whiteSpaceFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("  ");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_EMPTY_STRING_EXCEPTION)));
	}
	
	
	/**
	 * Description: Test Method for invalid first name - containing special character
	 * @throws Exception object
	 */
	@Test
	public void specialCharacterFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("great@!");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_SPECIAL_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid first name - too long
	 * @throws Exception object
	 */
	@Test
	public void tooLongFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("jessontangoanwewowewowewooheyeahwewoeodowoeasdjkajsdjasjkdhhqwe");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_TOO_LONG_EXCEPTION)));
	}
	

	/**
	 * Description: Test Method for invalid first name - empty
	 * @throws Exception object
	 */
	@Test
	public void emptyFirstName() throws Exception{
		//NOTE!!!!!!!
		//FirstName -> empty string(firstName="") not yet implemented
		//			-> null(wala gi set ang firstName) not yet implemented
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_EMPTY_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid first name - null
	 * @throws Exception object
	 */
	@Test
	public void nullFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_EMPTY_STRING_EXCEPTION)));
	}
	
	
	
	/**
	 * Description: Test Method for invalid last name - containing white space only
	 * @throws Exception object
	 */
	@Test
	public void whiteSpaceLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("  ");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_EMPTY_STRING_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid last name - containing numeric
	 * @throws Exception object
	 */
	@Test
	public void numericLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Sama23");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_NUMERIC_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid last name - containing special character
	 * @throws Exception object
	 */
	@Test
	public void specialCharacterLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Sama@great!");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_SPECIAL_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid last name - too long
	 * @throws Exception object
	 */
	@Test
	public void tooLongLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Samaasedeqweijfashfsjkvnajklvnaksjerhkljfhasjkdfbasjkldgvhasjkldfhasjkdf");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_TOO_LONG_EXCEPTION)));
	}
		

	/**
	 * Description: Test Method for invalid last name - empty
	 * @throws Exception object
	 */
	@Test
	public void emptyLastName() throws Exception{
		//NOTE!!!!!
		//LastName -> empty string(lastName="") not yet implemented
		//			-> null(wala gi set ang lastName) not yet implemented
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_EMPTY_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid last name - null
	 * @throws Exception object
	 */
	@Test
	public void nullLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_NULL_EMPTY_EXCEPTION)));
		
	}
	
	
	/**
	 * Description: Test Method for invalid address - too long
	 * @throws Exception object
	 */
	@Test
	public void tooLongAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("987-4223 Urna Square Garden St. Savannah Illinois 85794");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_TOO_LONG_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid address - containing only white space
	 * @throws Exception object
	 */
	@Test
	public void whiteSpaceAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("   ");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_EMPTY_STRING_EXCEPTION)));
	}
		

	/**
	 * Description: Test Method for invalid address - null
	 * @throws Exception object
	 */
	@Test
	public void nullAddress() throws Exception{
		//NOTE!!!!
		//address is null not yet implemented
		//address is empty(address="") is not yet implemented
		CustomerVO cust = new CustomerVO();
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_NULL_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid address - empty
	 * @throws Exception object
	 */
	@Test
	public void emptyAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_EMPTY_EMPTY_EXCEPTION)));
	}
	
	
	/**
	 * Description: Test Method for invalid gender - containing white space only
	 * @throws Exception object
	 */
	@Test
	public void whiteSpaceGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender(" ");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_EMPTY_EMPTY_EXCEPTION)));
	}
	
	/**
	 * Description: Test Method for invalid gender - character other than 'M' or 'F'
	 * @throws Exception object
	 */
	@Test
	public void notMorFGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("A");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid Gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid gender - containing special character
	 * @throws Exception object
	 */
	@Test
	public void specialCharacterGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("#");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid gender - containing numeric
	 * @throws Exception object
	 */
	@Test
	public void numericGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("15");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid gender - too long characters, LIMIT = 1, allowed values = 'M' or 'F'
	 * @throws Exception object
	 */
	@Test
	public void tooLongGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("MM");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	

	/**
	 * Description: Test Method for invalid gender - empty
	 * @throws Exception object
	 */
	@Test
	public void emptyGender() throws Exception{
		//NOTE!!!!
		//gender is null not yet implemented
		//gender is empty(address="") is not yet implemented
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_EMPTY_EMPTY_EXCEPTION)));
	}
	
	/**
	 * Description: Test Method for invalid gender - null
	 * @throws Exception object
	 */
	@Test
	public void nullGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_NULL_EMPTY_EXCEPTION)));
	}
	
	
	/**
	 * Description: Test Method for invalid age - not in range, below minimum
	 * @throws Exception object
	 */
	@Test
	public void lowAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("10");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NOT_IN_RANGE_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid age - not in range, above maximum
	 * @throws Exception object
	 */
	@Test
	public void highAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("150");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NOT_IN_RANGE_EXCEPTION)));
	}
	
	/**
	 * Description: Test Method for invalid age - containing alphabetic characters
	 * @throws Exception object
	 */
	@Test
	public void alphabeticAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("twenty");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NON_NUMERIC_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid age - containing special characters
	 * @throws Exception object
	 */
	@Test
	public void specialCharacterAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("@%");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NON_NUMERIC_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid age - null
	 * @throws Exception object
	 */
	@Test
	public void nullAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NULL_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for invalid age - containing whitespace only
	 * @throws Exception object
	 */
	@Test
	public void whiteSpaceAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("   ");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_EMPTY_EMPTY_EXCEPTION)));
	}
	
	/**
	 * Description: Test Method for invalid age - empty
	 * @throws Exception object
	 */
	@Test
	public void emptyAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_EMPTY_EMPTY_EXCEPTION)));
	}
	/**
	 * Description: Test Method for exception
	 * @throws Exception object
	 */
	@Test
	public void exceptionTest() throws Exception{
		
		CustomExceptions error = new CustomExceptions();
		String testStatement = "First name must not be empty.";
		//testStatement=error.FIRST_NAME_NULL_EMPTY_EXCEPTION;
		assertEquals(testStatement,error.sendException());
		
	}
	
	
	
}
