/*import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.bcexpress.controller.HomeController;
import com.bcexpress.domain.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringJUnit4ClassRunner.class)
public class CustomerSpringTest {
	
	private MockMvc mockMvc;	
	
	@InjectMocks
	private HomeController homeController;
	
	@Before
	public void setUp()throws Exception{
		mockMvc = MockMvcBuilders.standaloneSetup(homeController).build();
		
	}
	
	@Test
	public void testValidInputs() throws Exception {
		
		Customer customer = new Customer();
		customer.setAddress("cebu");
		customer.setFirstName("Jesson");
		customer.setLastName("Great");
		customer.setAge(3);
		
		ObjectMapper objectCustomer = new ObjectMapper();
		String jsonRequest = objectCustomer.writeValueAsString(customer);	
		
		mockMvc.perform(MockMvcRequestBuilders.get("/add")
		.content(jsonRequest)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("Customer information saved."))
		.andExpect(status().isCreated());
	}
	
	@Test
	public void testInvalidFirstName() throws Exception {
		
		Customer customer = new Customer();
		customer.setAddress("cebu");
		customer.setFirstName("Jesson1");
		customer.setLastName("Great");
		customer.setGender("M");
		customer.setAge(3);
		
		ObjectMapper objectCustomer = new ObjectMapper();
		String jsonRequest = objectCustomer.writeValueAsString(customer);		

		mockMvc.perform(MockMvcRequestBuilders.get("/add")
		.content(jsonRequest)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("First name must contain no numeric characters."))
		.andExpect(status().isBadRequest());
	}
	@Test
	public void testInvalidLastName() throws Exception {
		Customer customer = new Customer();
		customer.setAddress("cebu");
		customer.setFirstName("Jesson");
		customer.setLastName("Great@");
		customer.setGender("M");
		customer.setAge(3);
		
		ObjectMapper objectCustomer = new ObjectMapper();
		String jsonRequest = objectCustomer.writeValueAsString(customer);		

		mockMvc.perform(MockMvcRequestBuilders.get("/add")
		.content(jsonRequest)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("Last name must contain no special characters."))
		.andExpect(status().isBadRequest());
	}
	@Test
	public void testInvalidAddres() throws Exception {
		Customer customer = new Customer();
		customer.setAddress("cebu");
		customer.setFirstName("Jesson");
		customer.setLastName("Great@");
		customer.setGender("M");
		customer.setAge(3);
		
		ObjectMapper objectCustomer = new ObjectMapper();
		String jsonRequest = objectCustomer.writeValueAsString(customer);		

		mockMvc.perform(MockMvcRequestBuilders.get("/add")
		.content(jsonRequest)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("Last name must contain no special characters."))
		.andExpect(status().isBadRequest());
	}
	@Test
	public void testInvalidGender() throws Exception {
		Customer customer = new Customer();
		customer.setAddress("");
		customer.setFirstName("Jesson");
		customer.setLastName("Great@");
		customer.setGender("Male");
		customer.setAge(3);
		
		ObjectMapper objectCustomer = new ObjectMapper();
		String jsonRequest = objectCustomer.writeValueAsString(customer);		

		mockMvc.perform(MockMvcRequestBuilders.get("/add")
		.content(jsonRequest)
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("Last name must contain no special characters."))
		.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testValidCustomerId() throws Exception {

		
		mockMvc.perform(MockMvcRequestBuilders.get("/getCustomer/{id}")
		.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.content().string("Record found."))
		.andExpect(status().isOk());
	}
	
}
*/