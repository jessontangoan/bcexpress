package com.bcexpress.junitTest;



import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.bcexpress.controller.CustomerController;
import com.bcexpress.exceptions.CustomExceptions;
import com.bcexpress.facade.CustomerFacade;
import com.bcexpress.model.CustomerVO;
import com.fasterxml.jackson.databind.ObjectMapper;


/*
 * Description: A class to test saving of customer object
 * Date created : 01-29-2019
 * Author: TeamJ3
 * 
 */

public class SaveCustomerTest {
	
	private MockMvc mockMvc;
	private ObjectMapper objectMapper;
	
	@Mock
	private CustomerFacade customerFacade;
	
	@InjectMocks
	private CustomerController customerController;
	
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(customerController).build();
		objectMapper = new ObjectMapper();
		
		
	}
	
	
	
	@Test
	public void validFirstNameTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setFirstName("Bilbo Abelik Guulime");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	
	@Test
	public void validAddressTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("M");
		cust.setAge("23");
		cust.setFirstName("Bilbo Abelik Guulime");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	@Test
	public void validAddressTest2() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("8979 Wilson Circle Homestead");
		cust.setAge("23");
		cust.setFirstName("Bilbo Abelik Guulime");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	
	
	@Test
	public void validGenderTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("8979 Wilson Circle Homestead");
		cust.setAge("23");
		cust.setFirstName("cebu");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	@Test
	public void minimumLengthFirstNameTest() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setFirstName("B");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonrequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonrequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.message", is("Customer Successfully Created")));
		
	}
	
	
	
	//FIRSTNAME
	
	//save method based on UTP
	//INVALID firstName, firstName is null
	@Test
	public void numericFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setFirstName("bilbo23");
		cust.setGender("M");
		cust.setLastName("Baggins");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_NUMERIC_EXCEPTION)));
	
	}
	
	
	
	@Test
	public void whiteSpaceFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("  ");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_EMPTY_STRING_EXCEPTION)));
	}
	
	
	
	@Test
	public void specialCharacterFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("great@!");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_SPECIAL_EXCEPTION)));
	}
	
	@Test
	public void tooLongFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("jessontangoanwewowewowewooheyeahwewoeodowoeasdjkajsdjasjkdhhqwe");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_TOO_LONG_EXCEPTION)));
	}
	
	//NOTE!!!!!!!
	//FirstName -> empty string(firstName="") not yet implemented
	//			-> null(wala gi set ang firstName) not yet implemented
	
	@Test
	public void emptyFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("");
		cust.setGender("M");
		cust.setLastName("Sama");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_EMPTY_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void nullFirstName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("Cebu");
		cust.setAge("23");
		cust.setGender("M");
		cust.setLastName("Great");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid firstName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.FIRST_NAME_EMPTY_STRING_EXCEPTION)));
	}
	
	
	
	
	
	
	//LASTNAME
	@Test
	public void whiteSpaceLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("  ");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_EMPTY_STRING_EXCEPTION)));
	}
	
	@Test
	public void numericLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Sama23");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_NUMERIC_EXCEPTION)));
	}
	
	@Test
	public void specialCharacterLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Sama@great!");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_SPECIAL_EXCEPTION)));
	}
	
	@Test
	public void tooLongLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Samaasedeqweijfashfsjkvnajklvnaksjerhkljfhasjkdfbasjkldgvhasjkldfhasjkdf");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_TOO_LONG_EXCEPTION)));
	}
	
	
	//NOTE!!!!!
	//LastName -> empty string(lastName="") not yet implemented
	//			-> null(wala gi set ang lastName) not yet implemented
	
	@Test
	public void emptyLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_EMPTY_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void nullLastName() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("madagascar");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid lastName")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.LAST_NAME_NULL_EMPTY_EXCEPTION)));
		
	}
	
	
	
	
	//ADDRESS
	@Test
	public void tooLongAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("987-4223 Urna Square Garden St. Savannah Illinois 85794");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_TOO_LONG_EXCEPTION)));
	}
	
	@Test
	public void whiteSpaceAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("   ");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_EMPTY_STRING_EXCEPTION)));
	}
	
	
	//NOTE!!!!
	//address is null not yet implemented
	//address is empty(address="") is not yet implemented
	@Test
	public void nullAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_NULL_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void emptyAddress() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid address")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.ADDRESS_EMPTY_EMPTY_EXCEPTION)));
	}
	
	
	
	
	
	
	
	//GENDER
	@Test
	public void whiteSpaceGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender(" ");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_EMPTY_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void notMorFGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("A");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid Gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void specialCharacterGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("#");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void numericGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("15");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void tooLongGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("MM");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_INVALID_EMPTY_EXCEPTION)));
	}
	
	//NOTE!!!!
	//gender is null not yet implemented
	//gender is empty(address="") is not yet implemented
	
	@Test
	public void emptyGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setGender("");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_EMPTY_EMPTY_EXCEPTION)));
	}
	
	
	@Test
	public void nullGender() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("30");
		cust.setFirstName("Saitama");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid gender")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.GENDER_NULL_EMPTY_EXCEPTION)));
	}
	
	
	
	
	
	
	
	//AGE
	@Test
	public void lowAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("10");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NOT_IN_RANGE_EXCEPTION)));
	}
	
	@Test
	public void highAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("150");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NOT_IN_RANGE_EXCEPTION)));
	}
	
	
	@Test
	public void alphabeticAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("twenty");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NON_NUMERIC_EXCEPTION)));
	}
	
	@Test
	public void specialCharacterAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("@%");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NON_NUMERIC_EXCEPTION)));
	}
	
	@Test
	public void nullAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_NULL_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void whiteSpaceAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("   ");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_EMPTY_EMPTY_EXCEPTION)));
	}
	
	
	@Test
	public void emptyAge() throws Exception{
		CustomerVO cust = new CustomerVO();
		cust.setAddress("cebu");
		cust.setAge("");
		cust.setFirstName("Saitama");
		cust.setGender("M");
		cust.setLastName("Hero");
		cust.setId(100);
		
		String jsonRequest = objectMapper.writeValueAsString(cust);
		mockMvc.perform(post("/service/save").content(jsonRequest).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().is(400))
				.andExpect(jsonPath("$.message", is("Invalid age")))
				.andExpect(jsonPath("$.description", is(CustomExceptions.AGE_EMPTY_EMPTY_EXCEPTION)));
	}
	
	@Test
	public void exceptionTest() throws Exception{
		
		CustomExceptions error = new CustomExceptions();
		String testStatement = "First name must not be empty.";
		//testStatement=error.FIRST_NAME_NULL_EMPTY_EXCEPTION;
		assertEquals(testStatement,error.sendException());
		
	}
	
	
	
}
